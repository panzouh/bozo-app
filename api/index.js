const bodyParser = require('body-parser');
const express = require('express');

const app = express();
require('./models/dbConfig');
const postsRoutes = require('./controllers/postsControllers');
const mongoose = require('mongoose');
// mongoose.set('useFindAndModify', false);

app.use(bodyParser.json());
app.use('/', postsRoutes);

app.listen(3000, () => console.log('Server listening on 3000'));