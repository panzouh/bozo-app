const express = require('express');
const router = express.Router();
const ObjectId = require('mongoose').Types.ObjectId;

const { PostsModels } = require ('../models/postsModels');

router.get('/', (_, res)  => {
    PostsModels.find((err, docs) => {
        if (!err)
            res.send(docs);
        else
            console.log("Errors finding data" + err);
    }).sort([['balance', -1]]);
});

router.get('/live', async (_req, res, _next) => {
    // optional: add further things to check (e.g. connecting to dababase)
    const healthcheck = {
        uptime: process.uptime(),
        message: 'OK',
        timestamp: Date.now()
    };
    try {
        res.send(healthcheck);
    } catch (err) {
        healthcheck.message = e;
        res.status(503).send();
    }
});

router.post('/', (req, res) => {
    const newRecord = new PostsModels({
        name: req.body.name,
        balance: req.body.balance
    });
    newRecord.save((err, docs)=> {
        if (!err)
            res.send(docs);
        else
            console.log("Error creating new data" + err);
    });
});

router.put('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send('Id unknown' + req.params.id);
    const UpdateRecord = {
        balance: req.body.balance
    };
    PostsModels.findByIdAndUpdate(
        req.params.id,
        { $set: UpdateRecord },
        { new: true },
        (err, docs) => {
            if (!err)
                res.send(docs);
            else
                console.log("Update error" + err);
        }
    );
});

router.delete('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send('Id unknown' + req.params.id);
    PostsModels.findByIdAndRemove(
        req.params.id,
        (err, docs) => {
            if (!err)
                res.send(docs);
            else
                console.log("Delete error" + err);
        }
    )

});

module.exports = router;