# BOZO API

Docker repository : [Repository](https://hub.docker.com/repository/docker/panzouh/bozo-api)

![Docker Image Size (tag)](https://img.shields.io/docker/image-size/panzouh/bozo-api/latest)
![Docker Image Version (latest by date)](https://img.shields.io/docker/v/panzouh/bozo-api)

## Description

A simple API in nodeJS to interact with MongoDB.

### Dependencies

- [Node version 14](https://nodejs.org/es/blog/release/v14.0.0/)
- [Mongoose v6.0.9](https://www.npmjs.com/package/mongoose)
- [Nodemon v2.0.13](https://www.npmjs.com/package/nodemon)
- [Express v4.17.1](https://www.npmjs.com/package/express)
- [Body Parser](https://www.npmjs.com/package/body-parser)

### Environment

| Variable | Default value |
| -------- | ----- |
| BOZO_DB | "mongodb://localhost:27017/bozo-api" |
| BOZO_DB_USER | "root" |
| BOZO_DB_PASSWORD | "root" |

### Postman tests export

[Postman tests exports](postman/bozo-app-export.json)

## Routes

### Get users (GET method)

#### Get command

```sh
$: curl http://<api>:3000
```

#### Get response

```json
[
    {
        "_id": "616807aef68baf963e479cd2",
        "name": "test",
        "balance": 2,
        "date": "2021-10-14T10:34:22.126Z",
        "__v": 0
    },
    {
        "_id": "61680808943985f96580718d",
        "name": "test2",
        "balance": 1,
        "date": "2021-10-14T10:35:52.324Z",
        "__v": 0
    }
]
```

### Create user (POST method)

#### Post command

```sh
$: curl -X POST http://<api>:3000
   -H 'Content-Type: application/json'
   -d '{"name":"<user-name>","balance": 1}'
```

#### Post response

```json
{
    "name": "<user-name>",
    "balance": 1,
    "_id": "<user-id>",
    "date": "2021-10-14T11:38:35.359Z",
    "__v": 0
}
```

### Update user "Bozo coins" balance (PUT method)

#### Put command

```sh
$: curl -X PUT http://<api>:3000/<user-id>
    -H "Content-Type: application/json"
    -d '{"balance":<new-balance>}'
```

#### Put response

```json
{
    "_id": "<user-id>",
    "name": "<user-name>",
    "balance": "<user-balance>",
    "date": "2021-10-14T10:35:52.324Z",
    "__v": 0
}
```

### Delete user (DELETE method)

#### Delete command

```sh
$: curl -X DELETE http://<api>:3000/<user-id>
```

#### Delete response

```json
{
    "_id": "<user-id>",
    "name": "<user-name>",
    "balance": "<user-balance>",
    "date": "2021-10-14T10:35:52.324Z",
    "__v": 0
}
```
