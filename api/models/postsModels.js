const mongoose = require('mongoose');

const PostsModels = mongoose.model(
    "bozzo-api",
    {
        name: {
            type: String,
            required: true
        },
        balance: {
            type: Number,
            min: 1,
            required: true
        },
        date: {
            type: Date,
            default: Date.now
        }
    },
    "posts"
);

module.exports = { PostsModels };