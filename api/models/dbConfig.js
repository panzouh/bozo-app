const mongoose = require('mongoose');

mongoose.connect(
    process.env.BOZO_DB,
    {
        authSource: "admin",
        auth: {
            username: process.env.BOZO_DB_USER,
            password: process.env.BOZO_DB_PASSWORD
        },
        useNewUrlParser: true,
        useUnifiedTopology: true
    },
    (err) => {
        if (!err) 
            console.log("MongoDB connected");
        else 
            console.log("Connection error " + err);
    }
)