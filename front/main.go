package main

import (
	"bytes"
	"encoding/json"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/heptiolabs/healthcheck"
)

// App vars
var bozoApi = os.Getenv("BOZO_API")
var bozoFrontPort = ":8888"
var bozoHealthPort = ":8989"

// Tpl vars
var tpl *template.Template

// Db scheme
type Player struct {
	Id      string `json:"_id"`
	Name    string
	Balance int
	Date    time.Time
}

// Get players
func getPlayers() []byte {
	resp, err := http.Get(bozoApi)
	if err != nil {
		log.Fatalln(err)
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}
	return body
}

func init() {
	tpl = template.Must(template.ParseGlob("./templates/*.html"))
}

func main() {
	main := http.NewServeMux()
	main.HandleFunc("/", index)
	main.HandleFunc("/add-coin", addBalance)
	main.HandleFunc("/del-coin", removeBalance)
	main.HandleFunc("/add-player", addPlayer)
	main.HandleFunc("/del-player", delPlayer)
	main.Handle("/src/", http.StripPrefix("/src/", http.FileServer(http.Dir("src"))))

	health := healthcheck.NewHandler()
	health.AddReadinessCheck(
		"api",
		healthcheck.HTTPGetCheck(bozoApi, 3*time.Second),
	)
	health.AddLivenessCheck(
		"goroutine-threshold",
		healthcheck.GoroutineCountCheck(100),
	)

	// Running metrics server
	go func() {
		log.Printf("Metrics server started on %s", bozoHealthPort)
		log.Fatalln(http.ListenAndServe(bozoHealthPort, health))
	}()

	// Running main server
	log.Printf("Main server started on %s", bozoFrontPort)
	log.Fatalln(http.ListenAndServe(bozoFrontPort, main))

}

func index(w http.ResponseWriter, r *http.Request) {
	reqBody := getPlayers()
	var d []Player
	json.Unmarshal([]byte(reqBody), &d)

	if err := tpl.ExecuteTemplate(w, "index.html", d); err != nil {
		log.Fatalln(err)
	}
}

func addPlayer(w http.ResponseWriter, r *http.Request) {
	name := r.URL.Query().Get("player")

	if name != "" {
		d := map[string]string{"name": name, "balance": "1"}
		client := &http.Client{}

		json, err := json.Marshal(d)
		if err != nil {
			log.Fatalln(err)
		}

		req, err := http.NewRequest(http.MethodPost, bozoApi, bytes.NewBuffer(json))
		if err != nil {
			log.Fatalln(err)
		}

		req.Header.Set("Content-Type", "application/json; charset=utf-8")

		resp, err := client.Do(req)
		if err != nil {
			panic(err)
		}
		log.Printf("New player added | Response code: %d | For name: %s balance: %s", resp.StatusCode, name, "1")
		http.Redirect(w, r, "/", http.StatusSeeOther)
	}
}

func delPlayer(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Query().Get("id")
	if id != "" {
		d := map[string]string{"id": id}

		client := &http.Client{}

		json, err := json.Marshal(d)
		if err != nil {
			log.Fatalln(err)
		}

		req, err := http.NewRequest(http.MethodDelete, bozoApi+id, bytes.NewBuffer(json))
		if err != nil {
			log.Fatalln(err)
		}

		req.Header.Set("Content-Type", "application/json; charset=utf-8")

		resp, err := client.Do(req)
		if err != nil {
			log.Fatalln(err)
		}
		log.Printf("Player removed | Response code: %d | For id: %s", resp.StatusCode, id)
		http.Redirect(w, r, "/", http.StatusSeeOther)
	} else {
		log.Println("Id can't be null")
		http.Redirect(w, r, "/", http.StatusSeeOther)
	}
}

func addBalance(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Query().Get("id")
	balance := r.URL.Query().Get("balance")
	if id != "" && balance != "" {
		intBalance, _ := strconv.Atoi(balance)
		intBalance++
		if intBalance < 1 {
			log.Printf("Balance can't be < 1 | Request not executed For id: %s balance: %s", id, balance)
			http.Redirect(w, r, "/", http.StatusSeeOther)
		} else {
			balance := strconv.Itoa(intBalance)

			d := map[string]string{"balance": balance}

			client := &http.Client{}

			json, err := json.Marshal(d)
			if err != nil {
				panic(err)
			}

			req, err := http.NewRequest(http.MethodPut, bozoApi+id, bytes.NewBuffer(json))
			if err != nil {
				panic(err)
			}

			req.Header.Set("Content-Type", "application/json; charset=utf-8")

			resp, err := client.Do(req)
			if err != nil {
				panic(err)
			}
			log.Printf("Balance added | Response code: %d | For id: %s balance: %s", resp.StatusCode, id, balance)
			http.Redirect(w, r, "/", http.StatusSeeOther)
		}
	}
}

func removeBalance(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Query().Get("id")
	balance := r.URL.Query().Get("balance")
	if id != "" && balance != "" {
		intBalance, _ := strconv.Atoi(balance)
		intBalance--
		if intBalance < 1 {
			log.Printf("Balance can't be < 1 | Request not executed For id: %s balance: %s", id, balance)
			http.Redirect(w, r, "/", http.StatusSeeOther)
		} else {
			balance := strconv.Itoa(intBalance)
			d := map[string]string{"balance": balance}

			client := &http.Client{}

			json, err := json.Marshal(d)
			if err != nil {
				panic(err)
			}

			req, err := http.NewRequest(http.MethodPut, bozoApi+id, bytes.NewBuffer(json))
			if err != nil {
				panic(err)
			}

			req.Header.Set("Content-Type", "application/json; charset=utf-8")

			resp, err := client.Do(req)
			if err != nil {
				panic(err)
			}
			log.Printf("Balance removed | Response code: %d | For id: %s balance: %s", resp.StatusCode, id, balance)
			http.Redirect(w, r, "/", http.StatusSeeOther)

		}
	}
}
