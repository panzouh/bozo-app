# BOZO FRONT

Docker repository : [Repository](https://hub.docker.com/repository/docker/panzouh/bozo-front)

![Docker Image Size (tag)](https://img.shields.io/docker/image-size/panzouh/bozo-front/latest)
![Docker Image Version (latest by date)](https://img.shields.io/docker/v/panzouh/bozo-front)

## Description

A simple front in Golang to interact with [Bozo-Api](../api), served by html/template and net/http.

### Dependencies

- [Go 1.16](https://golang.org/doc/go1.16)
- [Bytes](https://pkg.go.dev/bytes)
- [Encoding/json](https://pkg.go.dev/encoding/json)
- [Html/template](https://pkg.go.dev/html/template)
- [Io/ioutil](https://pkg.go.dev/io/ioutil)
- [Log](https://pkg.go.dev/log)
- [Net/http](https://pkg.go.dev/net/http)
- [Os](https://pkg.go.dev/os)
- [Strconv](https://pkg.go.dev/strconv)
- [Time](https://pkg.go.dev/time)

### Environment

| Variable | Default value |
| -------- | ----- |
| BOZO_API | "http://localhost:3000" |

## Routes

### "/"

Index page served by [html template](templates/index.html), retrives player list.

### Interface

![Interface](img/Interface.png)

### "/add-player"

Add a player from the scoreboard.

#### Add player from shell

```sh
curl http://<front>/add-player?player=<player-name>
```

#### Add player from interface

![Interface](img/Add-player.png)

### "/del-player"

Remove a player from the scoreboard.

#### Delete player from shell

```sh
curl http://<front>/del-player?id=<player-id>
```

#### Delete player from interface

![Interface](img/Del-player.png)

### "/add-coin"

Add a "Bozo coin" to a player.

#### Add coin from shell

```sh
curl http://<front>/add-coin?id=<player-id>?balance=<player-balance>
```

#### Add coin from interface

![Interface](img/Add-coin.png)

### "/del-coin"

Remove a "Bozo coin" to a player.

#### Remove coin from shell

```sh
curl http://<front>/del-coin?id=<player-id>?balance=<player-balance>
```

#### Remove coin from interface

![Interface](img/Del-coin.png)
