# BOZO TESTS

## Requirements

- [Docker](https://docs.docker.com/get-docker/)
- [Docker Compose](https://docs.docker.com/compose/install/)

## Description

Creates a MongoDB server.

| Variable | Value |
| -------- | ----- |
| Admin username | root |
| Admin password | root |
| Application DB | bozo-api |

Auth chain :

```sh
mongodb://root:root@mongo:27017/
```

Test Api :

```sh
$: cd tests/ && docker-compose -f test-stack.yml up -d
$: cd ../api && npm test
```

Visualize datas :

Navigate to [Mongo-Express](http://localhost:8081)
