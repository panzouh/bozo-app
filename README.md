# BOZO BUNDLE

## Description

Rank your co-workers by giving them a "Bozo coin" for each of their stupidities.

## Refs

- [Front](front/)
- [Api](api/)
- [Database](db/)
- [Helm](helm/)
- [Test](tests/)

## Installation

See [Helm Chart](./helm).

## Roadmap

### Front

    * [ ] Add the possibility to filter cols
    * [ ] Add user profile page
    * [ ] Add trophies
    * [ ] Add Docker hub README

### Api

    * [ ] Add authentication
    * [ ] Profile scheme
    * [ ] Add Docker hub README

### Helm

    * [X] Add Helm chart
    * [X] Work on README
    * [X] Work on Helm Docker img (pre-install deps)
    * [ ] Add Docker hub README

## [License](LICENSE.md)
