# BOZO HELM

Here is the official Helm chart for the Bozo App.

## Installation

```sh
helm repo add danstonkube https://harbor.danstonkube.fr/chartrepo/library 
```

```sh
helm install bozo-app danstonkube/bozo-app
```

## Front vars

Front documentation here [Bozo-Front](../front).

| Variable | Default value | Description |
| -------- | ------------- | ----------- |
| imagePullPolicy | Always | Pull [strategy](https://kubernetes.io/docs/concepts/containers/images/#image-pull-policy) |
| labels | nil | Labels added to front resources (deploy, ingress, service) |
| podAnnotations | nil | Annotations to add to the pod like metrics scraping (not implemented yet) |
| ingress.class | nil | Define your IngressClass |
| ingress.enabled | false | Define if you want to create an ingress |
| ingress.annotations | nil | Define your ingress annotations |
| ingress.hosts | chart-example.local | Define host access |
| ingress.tls | nil | Define an existing tls secret or a new one |

## API vars

Api documentation here  [Bozo-Api](../api).

| Variable | Default value | Description |
| -------- | ------------- | ----------- |
| imagePullPolicy | Always | Pull [strategy](https://kubernetes.io/docs/concepts/containers/images/#image-pull-policy) |
| labels | nil | Labels added to API resources (deploy, ingress, service) |
| podAnnotations | nil | Annotations to add to the pod like metrics scraping (not implemented yet) |
| ingress.class | nil | Define your IngressClass |
| ingress.enabled | false | Define if you want to create an ingress |
| ingress.annotations | nil | Define your ingress annotations |
| ingress.hosts | chart-example.local | Define host access |
| ingress.tls | nil | Define an existing tls secret or a new one |

## MongoDB vars

See [MongoDB Bitnami Chart](https://github.com/bitnami/charts/tree/master/bitnami/mongodb).
